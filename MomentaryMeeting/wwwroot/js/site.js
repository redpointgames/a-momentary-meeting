﻿let targetText = window.authenticated ? selectGlobalText("post_login") : selectGlobalText("intro");
if (targetText === undefined) {
    targetText = "The text fragment is unknown.";
}
let currentText = "";
let currentInput = "";
let inputHistory = [];
let inputHistoryIterator = -1;
let cursorPosition = 0;
let timerMsRemaining = 0;
let authenticated = window.authenticated;
let ws = null;
let textSpeedModifier = 1;
let textSpeedToggle = 0;
let sayAppend = false;

let pendingMusicChange = null;
let currentTrackId, currentMeetingTrackId;
let currentTrackName = "void";
let currentInMeeting = false;
let audioTracks = {
    void: new Howl({
        src: ["/audio/void.ogg"],
        loop: true,
        volume: 0.0,
    }),
    void_meeting: new Howl({
        src: ["/audio/void.ogg"],
        loop: true,
        volume: 0.0,
    }),
    town: new Howl({
        src: ["/audio/town.ogg"],
        loop: true,
        volume: 0.0,
    }),
    town_meeting: new Howl({
        src: ["/audio/town_meeting.ogg"],
        loop: true,
        volume: 0.0,
    }),
    beach: new Howl({
        src: ["/audio/beach.ogg"],
        loop: true,
        volume: 0.0,
    }),
    beach_meeting: new Howl({
        src: ["/audio/beach_meeting.ogg"],
        loop: true,
        volume: 0.0,
    }),
    caves: new Howl({
        src: ["/audio/caves.ogg"],
        loop: true,
        volume: 0.0,
    }),
    caves_meeting: new Howl({
        src: ["/audio/caves_meeting.ogg"],
        loop: true,
        volume: 0.0,
    }),
    forest: new Howl({
        src: ["/audio/forest.ogg"],
        loop: true,
        volume: 0.0,
    }),
    forest_meeting: new Howl({
        src: ["/audio/forest_meeting.ogg"],
        loop: true,
        volume: 0.0,
    }),
    castle: new Howl({
        src: ["/audio/castle.ogg"],
        loop: true,
        volume: 0.0,
    }),
    castle_meeting: new Howl({
        src: ["/audio/castle_meeting.ogg"],
        loop: true,
        volume: 0.0,
    }),
}
currentTrackId = audioTracks[currentTrackName].play();
currentMeetingTrackId = audioTracks[currentTrackName + "_meeting"].play();
audioTracks[currentTrackName].fade(0, 1, 1000, currentTrackId);

/**
 * Select one possible string from globalTextVariables for the specified key.
 * For example, selectGlobalText("intro") will randomly choose an intro string
 *
 * @param {any} key
 */
function selectGlobalText(key) {
    return window.globalTextVariables[key][Math.floor(window.globalTextVariables[key].length * Math.random())];
}

/**
 * Takes a character from the string at the given offset. For control
 * characters like {b}, it takes all 3 of the actual characters.
 * 
 * @param {any} inputString
 * @param {any} offset
 */
function takeNextCharacter(inputString, offset) {
    let chr = inputString[offset];
    if (chr === '{') {
        let buffer = chr;
        while (chr !== '}') {
            offset++;
            chr = inputString[offset];
            buffer += chr;
        }
        return buffer;
    }
    return chr;
}

/**
 * Handles the given string as input from the user.
 * 
 * @param {any} inputString
 */
function handleInput(inputString) {
    if (!authenticated) {
        if (inputString === "login") {
            location.href = "/login";
        } else if (inputString === "credit" || inputString === "credits") {
            targetText = selectGlobalText("credits");
            currentText = "";
        }
    } else if (ws !== null) {
        ws.send(inputString);
    }
}

let typingInterval = 30 / textSpeedModifier;
let baseAnimationTimestamp = performance.now();
let charactersTyped = 0;
let logo = null;
let logoFadeIn = 1;
const logoStepFadeInInterval = 120;
let logoCountdown = logoStepFadeInInterval;

function setTextSpeed(newTextSpeedModifier) {
    textSpeedModifier = newTextSpeedModifier;
    typingInterval = 30 / textSpeedModifier;
}

function cycleThroughTextSpeeds() {
    if (textSpeedToggle == 0) {
        setTextSpeed(2);
        $("#text_speed").html('Text speed: fast');
        textSpeedToggle++;
    } else if (textSpeedToggle == 1) {
        setTextSpeed(4);
        $("#text_speed").html('Text speed: very fast');
        textSpeedToggle++;
    } else {
        setTextSpeed(1);
        $("#text_speed").html('Text speed: normal');
        textSpeedToggle = 0;
    }
    $("#text_speed").blur();
    // rebase character speed timing
    baseAnimationTimestamp = performance.now();
    charactersTyped = 0;
}

function render(timestamp) {
    let targetCharactersTyped = (timestamp - baseAnimationTimestamp) / typingInterval;
    while (charactersTyped < targetCharactersTyped) {
        typeNextCharacter();
        charactersTyped++;
    }
    window.requestAnimationFrame(render);
}

window.onload = function () {
    window.requestAnimationFrame(render);
}

function typeNextCharacter() {
    if (timerMsRemaining > 0) {
        timerMsRemaining -= typingInterval * textSpeedModifier;
        if (timerMsRemaining < 0) {
            timerMsRemaining = 0;
        }
        return;
    }
    if (logoFadeIn < 1 && logo !== null) {
        if (logoCountdown > 0) {
            logoCountdown -= typingInterval;
            return;
        } else {
            logoFadeIn += 0.1;
            logo.style.opacity = logoFadeIn;
            logoCountdown = logoStepFadeInInterval;
            return;
        }
    }
    if (currentText !== targetText) {
        currentText += takeNextCharacter(targetText, currentText.length);
        // turn of input display
        if (!sayAppend) {
            $("#input_bar").hide();
        }

        let fadedCount = -1;
        let processingPosition = 0;
        let parentNodes = [];
        let currentNode = document.createTextNode("");
        let topLevelNodes = [currentNode];
        while (processingPosition < currentText.length) {
            let nextCharacter = takeNextCharacter(currentText, processingPosition);
            processingPosition += nextCharacter.length;
            if (nextCharacter === "{logo}") {
                currentNode = document.createElement("img");
                currentNode.src = "/logo_ingame.png";
                topLevelNodes.push(currentNode);
                logo = currentNode;
                if (processingPosition === currentText.length) {
                    logoFadeIn = 0;
                }
                logo.style.opacity = logoFadeIn;

                currentNode = document.createTextNode("");
                topLevelNodes.push(currentNode);
            } else if (nextCharacter === "{b}") {
                if (parentNodes.length === 0) {
                    // this is a top level node, we have to start a new <b> node at the top level.
                    // meanwhile, the current node is done.
                    currentNode = document.createElement("b");
                    topLevelNodes.push(currentNode);
                    let textNode = document.createTextNode("");
                    currentNode.appendChild(textNode);
                    parentNodes.push(currentNode);
                    currentNode = textNode;
                } else {
                    // we are currently in a text node inside another node. the last element of
                    // parentNodes is where what we need to create our new node under.
                    currentNode = document.createElement("b");
                    parentNodes[parentNodes.length - 1].appendChild(currentNode);
                    let textNode = document.createTextNode("");
                    currentNode.appendChild(textNode);
                    parentNodes.push(currentNode);
                    currentNode = textNode;
                }
            } else if (nextCharacter === "{/b}") {
                if (parentNodes.length > 0 && parentNodes[parentNodes.length - 1].nodeName.toLowerCase() == "b") {
                    // pop the last node off, create a new text node under the 
                    // parent higher up (or a top level node if there are no more parents)
                    parentNodes.pop();
                    currentNode = document.createTextNode("");
                    if (parentNodes.length === 0) {
                        topLevelNodes.push(currentNode);
                    } else {
                        parentNodes[parentNodes.length - 1].appendChild(currentNode);
                    }
                } else {
                    // invalid operation, ignore
                }
            } else if (nextCharacter === "{written}") {
                if (parentNodes.length === 0) {
                    currentNode = document.createElement("span");
                    currentNode.classList.add("written");
                    topLevelNodes.push(currentNode);
                    let textNode = document.createTextNode("");
                    currentNode.appendChild(textNode);
                    parentNodes.push(currentNode);
                    currentNode = textNode;
                } else {
                    currentNode = document.createElement("span");
                    currentNode.classList.add("written");
                    parentNodes[parentNodes.length - 1].appendChild(currentNode);
                    let textNode = document.createTextNode("");
                    currentNode.appendChild(textNode);
                    parentNodes.push(currentNode);
                    currentNode = textNode;
                }
            } else if (nextCharacter === "{/written}") {
                if (parentNodes.length > 0 && parentNodes[parentNodes.length - 1].nodeName.toLowerCase() === "span") {
                    parentNodes.pop();
                    currentNode = document.createTextNode("");
                    if (parentNodes.length === 0) {
                        topLevelNodes.push(currentNode);
                    } else {
                        parentNodes[parentNodes.length - 1].appendChild(currentNode);
                    }
                } else {
                    // invalid operation, ignore
                }
            } else if (nextCharacter === "{faded}") {
                fadedCount = 0;
            } else if (nextCharacter === "{/faded}") {
                fadedCount = -1;
            } else if (nextCharacter === "{1}" && processingPosition === currentText.length) {
                timerMsRemaining = 1000;
            } else if (nextCharacter === "{.5}" && processingPosition === currentText.length) {
                timerMsRemaining = 500;
            } else if (nextCharacter === "{.3}" && processingPosition === currentText.length) {
                timerMsRemaining = 300;
            } else if (nextCharacter === "{.2}" && processingPosition === currentText.length) {
                timerMsRemaining = 200;
            } else if (nextCharacter === "{.1}" && processingPosition === currentText.length) {
                timerMsRemaining = 100;
            } else if (nextCharacter[0] === "{") {
                // unknown control code, ignore
            } else {
                if (fadedCount >= 0) {
                    let opacity = 1 - (Math.min(fadedCount, 5) / 5);
                    if (parentNodes.length === 0) {
                        currentNode = document.createElement("span");
                        currentNode.style.opacity = opacity.toString();
                        topLevelNodes.push(currentNode);
                        let textNode = document.createTextNode("");
                        currentNode.appendChild(textNode);
                        parentNodes.push(currentNode);
                        currentNode = textNode;
                    } else {
                        currentNode = document.createElement("span");
                        currentNode.style.opacity = opacity.toString();
                        parentNodes[parentNodes.length - 1].appendChild(currentNode);
                        let textNode = document.createTextNode("");
                        currentNode.appendChild(textNode);
                        parentNodes.push(currentNode);
                        currentNode = textNode;
                    }

                    currentNode.textContent += nextCharacter;

                    parentNodes.pop();
                    currentNode = document.createTextNode("");
                    if (parentNodes.length === 0) {
                        topLevelNodes.push(currentNode);
                    } else {
                        parentNodes[parentNodes.length - 1].appendChild(currentNode);
                    }

                    fadedCount += 1;
                } else {
                    currentNode.textContent += nextCharacter;
                }
            }
        }

        $("#present").empty();
        $("#present").append(topLevelNodes);

        if (pendingMusicChange !== null) {
            if (pendingMusicChange.targetCharacterCount !== null &&
                currentText.length >= pendingMusicChange.targetCharacterCount) {
                changeTrack(pendingMusicChange.track, pendingMusicChange.inMeeting);
                pendingMusicChange = null;
            }
        }
    }
    else {
        // turn on input display
        $("#input_bar").show();
    }
};

window.addEventListener("keydown", function (ev) {
    if (ev.keyCode === 27) {   // check for esc before anything else, as it should always be available
        //currentText = targetText;
        while (currentText !== targetText)  // print all available text immediately when esc is pressed
        {
            timeMsRemaining = 0;
            typeNextCharacter();
        }
    }
    if (currentText !== targetText && !sayAppend) { //additionally checks if currently writing an append for say command so input is only hiding in other circumstances
        return; // do not want player typing while description is writing itself
    }
    //console.log("Key code: " + ev.keyCode); // for checking key codes when debugging input stuff
    if ((ev.keyCode >= 48 && ev.keyCode <= 57) ||
        (ev.keyCode >= 65 && ev.keyCode <= 90) ||
        (ev.keyCode === 32)) { // captires dash '-', period '.', and forward slash '/'
        let newChar = String.fromCharCode(ev.keyCode);
        if (!ev.shiftKey) {
            newChar = newChar.toLowerCase();
        }
        let add = true;
        if (newChar === ' ') {
            if (currentInput.length === 0 || currentInput.endsWith(" ")) {
                add = false;
            }
        }
        if (ev.keyCode === 57 && ev.shiftKey) newChar = "("; // left parenthesis
        if (ev.keyCode === 48 && ev.shiftKey) newChar = ")"; // right parenthesis
        if (ev.keyCode === 49 && ev.shiftKey) newChar = "!"; // exclamation mark
        if (add) {
            currentInput = currentInput + newChar;
            if (currentInput.length > 120) {
                currentInput = currentInput.substring(0, 120);
            }
        }
    } else if ((ev.keyCode >= 188 && ev.keyCode <= 191) || ev.keyCode === 186 || ev.keyCode === 59 || ev.keyCode === 173 || ev.keyCode === 222) {    // quick and dirty punctuation
        let newChar = "";

        if (ev.keyCode === 186 || (ev.keyCode === 59 && ev.shiftKey)) newChar = ":"; // colon
        if (ev.keyCode === 189 || ev.keyCode === 173) {
            if (ev.shiftKey) {
                newChar = "_"; // underscore
            } else {
                newChar = "-"; // dash
            }

        }
        if (ev.keyCode === 190) newChar = "."; // period
        if (ev.keyCode === 191) newChar = "/"; // forward slash
        if (ev.keyCode === 191 && ev.shiftKey) newChar = "?"; // question mark
        if (ev.keyCode === 222) newChar = "'"; // apostrophe
        if (ev.keyCode === 188) newChar = ","; // comma

        currentInput = currentInput + newChar;
        if (currentInput.length > 120) {
            currentInput = currentInput.substring(0, 120);
        }
    } else if (ev.keyCode === 8) { // backspace
        if (currentInput.length > 0) {
            currentInput = currentInput.substring(0, currentInput.length - 1);
        }
    } else if (ev.keyCode === 13) { // enter
        handleInput(currentInput);
        inputHistory.unshift(currentInput);
        currentInput = "";
    } else if (ev.keyCode === 38) { // up arrow
        if (inputHistory.length) {
            if (inputHistoryIterator < 0) {
                inputHistoryIterator = 0;
                currentInput = inputHistory[inputHistoryIterator];
            } else if (inputHistoryIterator >= 0 && inputHistoryIterator < inputHistory.length - 1) {
                inputHistoryIterator++;
                currentInput = inputHistory[inputHistoryIterator];
            }
        }
    } else if (ev.keyCode === 40) { // down arrow
        if (inputHistory.length) {
            if (inputHistoryIterator <= 0) {
                inputHistoryIterator = -1;
                currentInput = "";
            } else if (inputHistoryIterator < inputHistory.length) {
                inputHistoryIterator--;
                currentInput = inputHistory[inputHistoryIterator];
            }
        }
    } else {
        // allow key to propagate
        return;
    }
    $("#input").text(currentInput);
    ev.preventDefault();
    ev.stopPropagation();
})

function changeTrack(nextTrackName, nextInMeeting) {
    if (nextTrackName === currentTrackName &&
        nextInMeeting === currentInMeeting) {
        // no need to switch
        return;
    }

    // only do a full stop if the track is different
    if (nextTrackName !== currentTrackName) {
        // start fading out old track
        let storedTrackId = currentTrackId;
        let storedMeetingTrackId = currentMeetingTrackId;
        if (currentInMeeting) {
            console.log("fading out track: " + currentTrackName + " (meeting)");
            audioTracks[currentTrackName + "_meeting"].fade(1, 0, 1000, currentMeetingTrackId);
            audioTracks[currentTrackName + "_meeting"].once("fade", function (id) {
                console.log("stopping track: " + currentTrackName);
                audioTracks[currentTrackName].stop(storedTrackId);
                audioTracks[currentTrackName + "_meeting"].stop(storedMeetingTrackId);
            });
        } else {
            console.log("fading out track: " + currentTrackName + " (non-meeting)");
            audioTracks[currentTrackName].fade(1, 0, 1000, currentTrackId);
            audioTracks[currentTrackName].once("fade", function (id) {
                console.log("stopping track: " + currentTrackName);
                audioTracks[currentTrackName].stop(storedTrackId);
                audioTracks[currentTrackName + "_meeting"].stop(storedMeetingTrackId);
            });
        }

        // start new track
        console.log("starting next track: " + nextTrackName);
        currentTrackName = nextTrackName;
        if (audioTracks[currentTrackName] === undefined) {
            console.error("track named " + currentTrackName + " does not exist!");
            currentTrackName = "void";
        }
        audioTracks[currentTrackName].volume(0);
        audioTracks[currentTrackName + "_meeting"].volume(0);
        currentTrackId = audioTracks[currentTrackName].play();
        currentMeetingTrackId = audioTracks[currentTrackName + "_meeting"].play();
        if (nextInMeeting) {
            console.log("fading in track: " + currentTrackName + " (meeting)");
            audioTracks[currentTrackName + "_meeting"].fade(0, 1, 1000, currentMeetingTrackId);
        } else {
            console.log("fading in track: " + currentTrackName + " (non-meeting)");
            audioTracks[currentTrackName].fade(0, 1, 1000, currentTrackId);
        }
        currentInMeeting = nextInMeeting;
    } else {
        // otherwise, cross-fade between non-meeting and meeting tracks.
        if (nextInMeeting) {
            console.log("cross-fade track to " + currentTrackName + " (meeting)");
            audioTracks[currentTrackName].fade(1, 0, 1000, currentTrackId);
            audioTracks[currentTrackName + "_meeting"].fade(0, 1, 1000, currentMeetingTrackId);
        } else {
            console.log("cross-fade track to " + currentTrackName + " (non-meeting)");
            audioTracks[currentTrackName].fade(0, 1, 1000, currentTrackId);
            audioTracks[currentTrackName + "_meeting"].fade(1, 0, 1000, currentMeetingTrackId);
        }
        currentInMeeting = nextInMeeting;
    }
}

if (authenticated) {
    ws = new WebSocket("wss://" + location.host + "/ws");
    ws.addEventListener("open", function (event) {
    });
    ws.addEventListener("close", function (event) {
        changeTrack("void");
        targetText = selectGlobalText("disconnect");
        currentText = "";
    });
    ws.addEventListener("error", function (event) {
    });
    ws.addEventListener("message", function (event) {
        console.log(event.data);
        if (event.data.startsWith("settrack:")) {
            let components = event.data.split(":");
            let nextTrackName = components[1];
            let nextInMeeting = components[2].toLowerCase() === "true";
            pendingMusicChange = {
                track: nextTrackName,
                inMeeting: nextInMeeting,
                targetCharacterCount: null,
            };
        } else {
            // if append mode (starts with '>') append instead of replace target text
            if (event.data[0] === '>' || event.data[0] === '<') {
                sayAppend = false; // not a say append event
                if (event.data[0] === '<') { // using < as 'say append' character
                    sayAppend = true;
                }
                // this pending music change needs to happen once the target text has been typed out, pre-append
                if (pendingMusicChange !== null) {
                    pendingMusicChange.targetCharacterCount = targetText.length;
                }

                targetText += "\n\n" + event.data.substr(1);
            }
            else {
                sayAppend = false; // not a say append event
                if (pendingMusicChange !== null) {
                    changeTrack(pendingMusicChange.track, pendingMusicChange.inMeeting);
                    pendingMusicChange = null;
                }
                targetText = event.data;
                currentText = "";
            }
        }
    });
}
