﻿namespace MomentaryMeeting.Controllers
{
    using AspNet.Security.OAuth.Discord;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Hosting;
    using MomentaryMeeting.Models;
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;

    public class HomeController : Controller
    {
        private readonly IHostEnvironment _hostEnvironment;
        private readonly UserManager<UserModel> _userManager;
        private readonly SignInManager<UserModel> _signInManager;
        private static ulong _simulatedDiscordId = 1;

        public HomeController(
            IHostEnvironment hostEnvironment,
            UserManager<UserModel> userManager,
            SignInManager<UserModel> signInManager)
        {
            _hostEnvironment = hostEnvironment;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [Route("/")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("/login")]
        public async Task<IActionResult> Login([FromQuery] bool forceDiscord)
        {
            if (_hostEnvironment.IsDevelopment() && !forceDiscord)
            {
                // Pretend to do Discord login.
                var user = new UserModel
                {
                    DiscordId = _simulatedDiscordId,
                    Username = "stranger" + _simulatedDiscordId,
                };
                _simulatedDiscordId++;
                await _userManager.CreateAsync(user);
                await _signInManager.SignInAsync(user, false);
                return RedirectToAction(nameof(Index));
            }

            try
            {
                await _signInManager.Context.SignOutAsync(IdentityConstants.ApplicationScheme);
            }
            catch { }
            try
            {
                await _signInManager.Context.SignOutAsync(IdentityConstants.ExternalScheme);
            }
            catch { }

            var redirectUrl = "/login/callback";
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(DiscordAuthenticationDefaults.AuthenticationScheme, redirectUrl);
            return new ChallengeResult(DiscordAuthenticationDefaults.AuthenticationScheme, properties);
        }

        [Route("/login/error")]
        public IActionResult LoginError()
        {
            return Json("oops! login didn't work");
        }

        [Route("/login/callback")]
        public async Task<IActionResult> ExternalLoginCallback()
        {
            var loginInfo = await _signInManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction(nameof(LoginError));
            }

            if (loginInfo.LoginProvider != DiscordAuthenticationDefaults.AuthenticationScheme)
            {
                return RedirectToAction(nameof(LoginError));
            }

            var providerDisplayName = loginInfo.Principal.FindFirstValue(ClaimTypes.Name);

            var result = await _signInManager.ExternalLoginSignInAsync(loginInfo.LoginProvider, loginInfo.ProviderKey, isPersistent: false);

            if (result.Succeeded)
            {
                var user = await _signInManager.UserManager.FindByLoginAsync(loginInfo.LoginProvider, loginInfo.ProviderKey);

                user.Username = providerDisplayName;

                var success = (await _signInManager.UserManager.UpdateAsync(user)).Succeeded;

                if (!success)
                {
                    return RedirectToAction(nameof(LoginError));
                }

                return RedirectToAction(nameof(Index));
            }

            if (result.RequiresTwoFactor)
            {
                return RedirectToAction(nameof(LoginError));
            }
            if (result.IsLockedOut)
            {
                return RedirectToAction(nameof(LoginError));
            }
            else
            {
                var user = new UserModel
                {
                    DiscordId = ulong.Parse(loginInfo.ProviderKey),
                    Username = providerDisplayName,
                };

                var identityResult = await _userManager.CreateAsync(user);

                if (identityResult.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return RedirectToAction(nameof(LoginError));
                }
            }
        }
    }
}
