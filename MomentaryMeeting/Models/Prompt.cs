﻿namespace MomentaryMeeting.Models
{
    public class Prompt
    {
        public Condition Condition { get; set; }

        public string Text { get; set; }
    }
}
