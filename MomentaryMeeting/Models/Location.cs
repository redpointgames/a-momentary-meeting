﻿namespace MomentaryMeeting.Models
{
    using System.Collections.Generic;

    public class Location
    {
        // set the region name
        public string Region { get; set; }

        // if this location is at the edge of a region, set the edge name here
        public string Edge { get; set; }

        public List<Prompt> Prompt { get; set; }

        // key is primary name of inspectable (player can use the name to refer to it)
        public Dictionary<string, Inspectable> Inspectables { get; set; }

        public Dictionary<UserModel.CardinalDirection, List<Prompt>> ViewFrom { get; set; }

        public string ViewFromAny { get; set; }

        public bool NoSpawn { get; set; }
    }

    public class Inspectable
    {
        // additional names that the player can refer to this as
        public List<string> Aliases { get; set; }

        // same as locations, multiple prompts with conditions for when they are chosen
        public List<Prompt> Prompt { get; set; }
    }
}
