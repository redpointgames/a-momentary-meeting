﻿namespace MomentaryMeeting.Models
{
    using System.Collections.Generic;

    public class Condition
    {
        public ConditionType Type { get; set; }
        public List<ConditionArrangement> Arrangements { get; set; }
    }
}
