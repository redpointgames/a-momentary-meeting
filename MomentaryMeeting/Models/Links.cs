﻿namespace MomentaryMeeting.Models
{
    using System.Collections.Generic;

    public class Links
    {
        public List<string> Always { get; set; }
        // key is X=Y, where X is the name of the arrangement name
        // and Y is the arrangement value
        public Dictionary<string, List<string>> Arrangements { get; set; }
    }
}
