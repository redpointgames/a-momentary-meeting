﻿namespace MomentaryMeeting.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using YamlDotNet.Serialization;

    public class GlobalText : Dictionary<string, List<string>>
    {
        public Dictionary<string, List<string>> CloneWithUsernameReplacement(string username)
        {
            var result = new Dictionary<string, List<string>>();
            foreach (var kv in this)
            {
                var resultList = new List<string>();
                foreach (var k in kv.Value)
                {
                    resultList.Add(k.Replace("__NAME__", username));
                }
                result.Add(kv.Key, resultList);
            }
            return result;
        }

        // select a possible global_text substitution for the given key
        public string SelectFromList(string key)
        {
            return this[key][new Random().Next(this[key].Count)];
        }
    }
}
