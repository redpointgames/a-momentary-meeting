﻿namespace MomentaryMeeting.Models
{
    using System;
    using System.Collections.Generic;

    public class Database
    {
        public GlobalText GlobalText { get; set; }

        // keys are region names
        public Dictionary<string, Region> Regions { get; set; }

        // keys are arrangement names
        public Dictionary<string, Arrangement> Arrangements { get; set; }

        // keys are location names
        public Dictionary<string, Location> Locations { get; set; }

        // keys are condition type
        public Links Links { get; set; }

        public List<string> Include { get; set; }
    }
}
