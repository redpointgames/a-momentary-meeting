namespace MomentaryMeeting.Models
{
    using Microsoft.Extensions.Caching.Distributed;
    using MomentaryMeeting.Services;
    using System.Threading.Tasks;

    public class UserModel
    {
        public ulong DiscordId { get; set; }

        public string Username { get; set; }

        public string LocationName { get; private set; }

        public async Task SetLocationNameAsync(IDistributedCache distributedCache, string newLocation)
        {
            this.LocationName = newLocation;
            await distributedCache.SetStringAsync("loc-" + DiscordId, newLocation);
        }

        public enum CardinalDirection
        {
            North,
            East,
            South,
            West
        }

        public enum RelativeDirection
        {
            Forward,
            Right,
            Back,
            Left
        }

        public CardinalDirection CurrentDirection { get; set; }

        public bool InMeeting { get; set; }

        // input a PlayerMoves direction, outputs relative cardinal direction based on CurrentDirection variable
        public CardinalDirection RelativeCardinalDirection(RelativeDirection movementDirection)
        {
            return (CardinalDirection)((((int)CurrentDirection % 4) + (int)movementDirection) % 4);
        }

        public string DirectionAsString(IDatabaseApi databaseApi, CardinalDirection dir, bool lowercase)
        {
            var difference = (int)dir - (int)CurrentDirection;
            if (difference < 0) { difference += 4; }
            if (difference >= 4) { difference -= 4; }
            var localDirection = (RelativeDirection)difference;

            if (lowercase)
            {
                switch (localDirection)
                {
                    case RelativeDirection.Forward:
                        return databaseApi.Current.GlobalText.SelectFromList("direction_forward_under");
                    case RelativeDirection.Back:
                        return databaseApi.Current.GlobalText.SelectFromList("direction_behind_under");
                    case RelativeDirection.Left:
                        return databaseApi.Current.GlobalText.SelectFromList("direction_left_under");
                    case RelativeDirection.Right:
                        return databaseApi.Current.GlobalText.SelectFromList("direction_right_under");
                }
            }
            else
            {
                switch (localDirection)
                {
                    case RelativeDirection.Forward:
                        return databaseApi.Current.GlobalText.SelectFromList("direction_forward");
                    case RelativeDirection.Back:
                        return databaseApi.Current.GlobalText.SelectFromList("direction_behind");
                    case RelativeDirection.Left:
                        return databaseApi.Current.GlobalText.SelectFromList("direction_left");
                    case RelativeDirection.Right:
                        return databaseApi.Current.GlobalText.SelectFromList("direction_right");
                }
            }

            return "UNKNOWN DIRECTION!!!";
        }
    }
}
