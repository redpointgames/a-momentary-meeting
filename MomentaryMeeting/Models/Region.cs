﻿namespace MomentaryMeeting.Models
{
    using System.Collections.Generic;

    public class Region
    {
        public string Description { get; set; }

        /// <summary>
        /// The key is the edge type (as specifed on the location), 
        /// and the value is the region it can link to.
        /// </summary>
        public Dictionary<string, string> EdgeLinks { get; set; }
    }
}
