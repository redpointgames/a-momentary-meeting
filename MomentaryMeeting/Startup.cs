namespace MomentaryMeeting
{
    using Discord;
    using Discord.Rest;
    using Microsoft.AspNetCore.Authentication;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.HttpOverrides;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using MomentaryMeeting.Game;
    using MomentaryMeeting.Models;
    using MomentaryMeeting.Services;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.WebSockets;
    using System.Security.Claims;
    using System.Threading.Tasks;

    public class Startup
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _configuration;

        public Startup(IWebHostEnvironment env, IConfiguration configuration)
        {
            _env = env;
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IDatabaseApi, DefaultDatabaseApi>();
            services.AddSingleton<GameInstance, GameInstance>();
            services.AddSingleton<CommandParser, CommandParser>();
            services.AddSingleton<ArrangementGenerator, ArrangementGenerator>();
            services.AddSingleton<InteractionManager, InteractionManager>();
            services.AddSingleton<IWordFilterEngine, WordFilterEngine>();
            services.AddSingleton<IDiscordApi, DiscordApi>();

            if (_env.IsProduction())
            {
                services.AddSingleton<IHostedService, DatabaseRefreshOnStartup>();
            }

            services.AddSession(options =>
            {
                options.Cookie.Name = "LicenseManager.Session";
                options.IdleTimeout = TimeSpan.FromDays(3650);
            });

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });

            services.AddIdentityCore<UserModel>()
                .AddUserStore<UserStore>()
                .AddSignInManager();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = IdentityConstants.ApplicationScheme;
                options.DefaultChallengeScheme = IdentityConstants.ApplicationScheme;
                options.DefaultSignInScheme = IdentityConstants.ExternalScheme;
            })
                .AddCookie(IdentityConstants.ApplicationScheme, options =>
                {
                    options.Cookie.HttpOnly = true;
                    options.ExpireTimeSpan = TimeSpan.FromDays(30);

                    options.LoginPath = "/login";
                    options.LogoutPath = "/logout";
                    options.AccessDeniedPath = "/access-denied";
                    options.SlidingExpiration = true;
                })
                .AddDiscord(options =>
                {
                    options.ClientId = _configuration.GetValue<string>("Discord:ClientId");
                    options.ClientSecret = _configuration.GetValue<string>("Discord:ClientSecret");
                    options.CallbackPath = "/login/discord";
                    options.Scope.Add("identify");
                    options.Scope.Add("email");
                    options.Scope.Add("guilds.join");
                    options.SaveTokens = true;

                    options.Events.OnCreatingTicket = async ctx =>
                    {
                        List<AuthenticationToken> tokens = ctx.Properties.GetTokens().ToList();

                        var userId = ulong.Parse(ctx.Identity.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
                        var accessToken = tokens.First(x => x.Name == "access_token").Value;

                        var discordApi = ctx.HttpContext.RequestServices.GetRequiredService<IDiscordApi>();
                        var allowLogin = await discordApi.JoinGuildAndCheckPermittedToPlay(userId, accessToken);
                        if (!allowLogin)
                        {
                            ctx.Fail("You could not be authenticated with Discord. Ensure that you are not at the server limit and are not banned. Alternatively, join the Discord directly by going to https://discord.gg/wKR82eK.");
                        }
                    };
                })
                .AddExternalCookie();

            services.AddControllersWithViews().AddRazorRuntimeCompilation();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // We know we're always behind HTTPS.
            app.Use((context, next) =>
            {
                context.Request.Scheme = "https";
                return next();
            });

            app.UseDeveloperExceptionPage();

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseSession(new SessionOptions
            {
                IdleTimeout = TimeSpan.FromDays(3650)
            });

            app.UseRouting();

            app.UseWebSockets();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            app.Use(async (context, next) =>
            {
                if (context.Request.Path == "/ws")
                {
                    if (context.WebSockets.IsWebSocketRequest)
                    {
                        var signInManager = context.RequestServices.GetRequiredService<SignInManager<UserModel>>();

                        if (!signInManager.IsSignedIn(context.User))
                        {
                            context.Response.StatusCode = 403;
                            return;
                        }

                        var user = await signInManager.UserManager.GetUserAsync(context.User);

                        using (WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync())
                        {
                            var socketFinishedTcs = new TaskCompletionSource<object>();

                            var gameInstance = context.RequestServices.GetRequiredService<GameInstance>();
                            await gameInstance.AcceptPlayer(user, webSocket, socketFinishedTcs);

                            await socketFinishedTcs.Task;
                        }
                    }
                    else
                    {
                        context.Response.StatusCode = 400;
                    }
                }
                else if (context.Request.Path == "/reload")
                {
                    var db = context.RequestServices.GetRequiredService<IDatabaseApi>();
                    await db.Refresh();
                    context.Response.Redirect("/?reload=ok");
                }
                else
                {
                    await next();
                }
            });
        }
    }
}
