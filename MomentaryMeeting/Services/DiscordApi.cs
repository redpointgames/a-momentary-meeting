﻿namespace MomentaryMeeting.Services
{
    using Discord.Rest;
    using Discord.WebSocket;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public interface IDiscordApi
    {
        Task<bool> JoinGuildAndCheckPermittedToPlay(ulong userId, string userAccessToken);

        Task<bool> IsUserBanned(ulong userId);

        Task LogUserMessage(ulong userId, string contentPosted, bool flaggedByBadWordFilter);

        Task LogUserConnected(ulong userId);

        Task LogUserDisconnected(ulong userId);
    }


    public class DiscordApi : IDiscordApi
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<DiscordApi> _logger;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private DiscordSocketClient _socketClient;
        private DiscordRestClient _restClient;
        private HashSet<ulong> _banList;

        private const ulong _discordRedpointGuild = 837908566572531723;
        private const ulong _discordLogChannel = 838391753425682472;

        public DiscordApi(IConfiguration configuration, ILogger<DiscordApi> logger, IWebHostEnvironment webHostEnvironment)
        {
            _configuration = configuration;
            _logger = logger;
            _webHostEnvironment = webHostEnvironment;
            _banList = null;
        }

        private async Task<DiscordRestClient> GetClient()
        {
            if (_restClient != null)
            {
                return _restClient;
            }

            var botToken = _configuration.GetValue<string>("Discord:BotToken");
            if (!string.IsNullOrWhiteSpace(botToken))
            {
                var client = new DiscordRestClient();
                await client.LoginAsync(Discord.TokenType.Bot, botToken);
                _restClient = client;

                // we also have to have authenticated to a gateway at least once for the REST SendMessage endpoint to work...
                var socketClient = new DiscordSocketClient(new DiscordSocketConfig
                {
                    GatewayIntents = Discord.GatewayIntents.GuildBans,
                });
                socketClient.UserBanned += SocketClient_UserBanned;
                socketClient.UserUnbanned += SocketClient_UserBanned;
                await socketClient.LoginAsync(Discord.TokenType.Bot, botToken);
                await socketClient.StartAsync();
                _socketClient = socketClient;

                return client;
            }

            return null;
        }

        private Task SocketClient_UserBanned(SocketUser arg1, SocketGuild arg2)
        {
            // Ban list changed, flush cache and fetch bans at next check.
            _logger.LogInformation("The ban list was updated, and will be refreshed on next check.");
            _banList = null;
            return Task.CompletedTask;
        }

        public async Task<bool> JoinGuildAndCheckPermittedToPlay(ulong userId, string userAccessToken)
        {
            if (_banList != null && _banList.Contains(userId))
            {
                _logger.LogError("A banned user attempted to sign in and was prevented.");
                return false;
            }

            var client = await GetClient();
            if (client == null)
            {
                _logger.LogError("Discord API not available, can not authenticate user!");
                return false;
            }

            var guild = await client.GetGuildAsync(_discordRedpointGuild);
            if (guild == null)
            {
                _logger.LogError("Can not find Redpoint guild, unable to authenticate.");
                return false;
            }

            _banList = (await guild.GetBansAsync()).Select(x => x.User.Id).ToHashSet();
            if (_banList.Contains(userId))
            {
                _logger.LogError("A banned user attempted to sign in and was prevented.");
                return false;
            }

            var existingUser = await client.GetGuildUserAsync(_discordRedpointGuild, userId);
            if (existingUser == null)
            {
                var newUser = await guild.AddGuildUserAsync(userId, userAccessToken);

                if (newUser == null)
                {
                    _logger.LogError("The user could not be joined to the Redpoint guild, but they don't already exist in the guild.");
                    return false;
                }

                // Otherwise user is considered authenticated.
                return true;
            }

            // Otherwise user is considered authenticated.
            return true;
        }

        public async Task<bool> IsUserBanned(ulong userId)
        {
            if (_banList != null && _banList.Contains(userId))
            {
                _logger.LogError("Detected that a user was banned in the Discord!");
                return true;
            }

            var client = await GetClient();
            if (client == null)
            {
                _logger.LogError("Discord API not available, can not check user for ban!");
                return false;
            }

            var guild = await client.GetGuildAsync(_discordRedpointGuild);
            if (guild == null)
            {
                _logger.LogError("Can not find Redpoint guild, unable to authenticate.");
                return false;
            }

            _banList = (await guild.GetBansAsync()).Select(x => x.User.Id).ToHashSet();
            if (_banList.Contains(userId))
            {
                _logger.LogError("Detected that a user was banned in the Discord!");
                return true;
            }

            return false;
        }

        public async Task LogUserMessage(ulong userId, string contentPosted, bool flaggedByBadWordFilter)
        {
            var client = await GetClient();
            if (client == null)
            {
                return;
            }

            var guild = await client.GetGuildAsync(_discordRedpointGuild);
            if (guild == null)
            {
                return;
            }

            var textChannel = await guild.GetTextChannelAsync(_discordLogChannel);
            if (textChannel == null)
            {
                return;
            }

            if (flaggedByBadWordFilter)
            {
                await textChannel.SendMessageAsync(":face_with_symbols_over_mouth: <@" + userId + "> has their message flagged by bad word filter: `" + contentPosted.Replace("`", "") + "`");
            }
            else
            {
                await textChannel.SendMessageAsync("<@" + userId + "> sent: `" + contentPosted.Replace("`", "") + "`");
            }
        }

        public async Task LogUserConnected(ulong userId)
        {
            var client = await GetClient();
            if (client == null)
            {
                return;
            }

            var guild = await client.GetGuildAsync(_discordRedpointGuild);
            if (guild == null)
            {
                return;
            }

            var textChannel = await guild.GetTextChannelAsync(_discordLogChannel);
            if (textChannel == null)
            {
                return;
            }

            await textChannel.SendMessageAsync("<@" + userId + "> started playing.");
        }

        public async Task LogUserDisconnected(ulong userId)
        {
            var client = await GetClient();
            if (client == null)
            {
                return;
            }

            var guild = await client.GetGuildAsync(_discordRedpointGuild);
            if (guild == null)
            {
                return;
            }

            var textChannel = await guild.GetTextChannelAsync(_discordLogChannel);
            if (textChannel == null)
            {
                return;
            }

            await textChannel.SendMessageAsync("<@" + userId + "> disconnected.");
        }
    }
}
