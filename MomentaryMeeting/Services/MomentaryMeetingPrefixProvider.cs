﻿namespace MomentaryMeeting.Services
{
    using Redpoint.CloudFramework.Prefix;
    using System.Collections.Generic;

    public class MomentaryMeetingPrefixProvider : IPrefixProvider
    {
        public Dictionary<string, string> Prefixes => new Dictionary<string, string>();
    }
}
