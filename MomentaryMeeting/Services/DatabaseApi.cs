﻿namespace MomentaryMeeting.Services
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;
    using MomentaryMeeting.Models;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using YamlDotNet.Serialization;
    using YamlDotNet.Serialization.NamingConventions;

    public interface IDatabaseApi
    {
        Database Current { get; }

        Task Refresh();
    }

    public class DefaultDatabaseApi : IDatabaseApi
    {
        private readonly IDeserializer _deserializer;
        private readonly ILogger<DefaultDatabaseApi> _logger;
        private readonly IConfiguration _configuration;
        private Database _currentDatabase;

        public DefaultDatabaseApi(ILogger<DefaultDatabaseApi> logger, IConfiguration configuration)
        {
            _deserializer = new DeserializerBuilder()
                .WithNamingConvention(UnderscoredNamingConvention.Instance)
                .IgnoreUnmatchedProperties()
                .Build();
            _logger = logger;
            _configuration = configuration;

            _currentDatabase = Load(filename =>
            {
                using var stream = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream($"MomentaryMeeting.Data.{filename}"));
                return stream.ReadToEnd();
            });

            _logger.LogInformation("Database was loaded from disk.");
        }

        public Database Load(Func<string, string> getFileContents)
        {
            var newdb = new Database
            {
                Arrangements = new Dictionary<string, Arrangement>(),
                GlobalText = new GlobalText(),
                Include = new List<string> { "database.yaml" },
                Locations = new Dictionary<string, Location>(),
                Regions = new Dictionary<string, Region>(),
                Links = new Links()
            };
            newdb.Links.Always = new List<string>();
            newdb.Links.Arrangements = new Dictionary<string, List<string>>();

            while (newdb.Include.Any())
            {
                var filename = newdb.Include.First();
                _logger.LogInformation($"Loading {filename}");
                var temp = _deserializer.Deserialize<Database>(getFileContents(filename));
                temp.Arrangements?.ToList().ForEach(kv => newdb.Arrangements[kv.Key] = kv.Value);
                temp.GlobalText?.ToList().ForEach(kv => newdb.GlobalText[kv.Key] = kv.Value);
                temp.Locations?.ToList().ForEach(kv => newdb.Locations[kv.Key] = kv.Value);
                temp.Regions?.ToList().ForEach(kv => newdb.Regions[kv.Key] = kv.Value);
                if (temp.Links != null)
                {
                    temp.Links.Always?.ToList().ForEach(v => newdb.Links.Always.Add(v));
                    temp.Links.Arrangements?.ToList().ForEach(kv =>
                    {
                        if (!newdb.Links.Arrangements.Keys.Contains(kv.Key))
                        {
                            newdb.Links.Arrangements[kv.Key] = kv.Value;
                        }
                        else
                        {
                            newdb.Links.Arrangements[kv.Key].AddRange(kv.Value);
                        }
                    });
                }
                if (temp.Include != null)
                    newdb.Include.AddRange(temp.Include);
                newdb.Include.Remove(filename);
            }
            return newdb;
        }

        public Database Current => _currentDatabase;

        public async Task Refresh()
        {
            var accessToken = _configuration.GetValue<string>("GitLab:PrivateToken");
            if (!string.IsNullOrWhiteSpace(accessToken))
            {
                _logger.LogWarning("Unable to refresh database from GitLab, because the GitLab:PrivateToken configuration value isn't set.");
                return;
            }

            var files = Assembly.GetExecutingAssembly().GetManifestResourceNames().Where(n => n.EndsWith(".yaml")).Select(n => n[22..]);
            var contents = new Dictionary<string, string>();
            using var client = new HttpClient();
            client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", accessToken);
            foreach (var file in files)
            {
                contents[file] = await client.GetStringAsync($"https://a-momentary-meeting:53q25YXsaa7nkKaykgcE@gitlab.com/api/v4/projects/23987727/repository/files/{file}/raw?ref=main");
            }
            _currentDatabase = Load(filename => contents[filename]);
            _logger.LogInformation("Database was refreshed from GitLab repository.");
        }
    }

    public class DatabaseRefreshOnStartup : IHostedService
    {
        private readonly IDatabaseApi _databaseApi;

        public DatabaseRefreshOnStartup(IDatabaseApi databaseApi)
        {
            _databaseApi = databaseApi;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _databaseApi.Refresh();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
