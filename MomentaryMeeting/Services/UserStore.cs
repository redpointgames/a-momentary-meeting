﻿namespace MomentaryMeeting.Services
{
    using AspNet.Security.OAuth.Discord;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Caching.Distributed;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using MomentaryMeeting.Models;

    public class UserStore : IUserStore<UserModel>, IUserLoginStore<UserModel>
    {
        private readonly IDistributedCache _distributedCache;

        public UserStore(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }

        public void Dispose()
        {
        }

        public async Task<IdentityResult> CreateAsync(UserModel user, CancellationToken cancellationToken)
        {
            await _distributedCache.SetStringAsync(user.DiscordId.ToString(), user.Username);

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> UpdateAsync(UserModel user, CancellationToken cancellationToken)
        {
            await _distributedCache.SetStringAsync(user.DiscordId.ToString(), user.Username);

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> DeleteAsync(UserModel user, CancellationToken cancellationToken)
        {
            await _distributedCache.RemoveAsync(user.DiscordId.ToString());

            return IdentityResult.Success;
        }

        public async Task<UserModel> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            var username = await _distributedCache.GetStringAsync(userId);
            if (username == null)
            {
                return null;
            }
            return new UserModel
            {
                DiscordId = ulong.Parse(userId),
                Username = username,
            };
        }

        public Task<string> GetUserIdAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user.DiscordId.ToString());
        }

        #region Unsupported Username Methods

        public Task<UserModel> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            return FindByIdAsync(normalizedUserName, cancellationToken);
        }

        public Task<string> GetNormalizedUserNameAsync(UserModel user, CancellationToken cancellationToken)
        {
            return GetUserIdAsync(user, cancellationToken);
        }

        public Task<string> GetUserNameAsync(UserModel user, CancellationToken cancellationToken)
        {
            return GetUserIdAsync(user, cancellationToken);
        }

        public Task SetNormalizedUserNameAsync(UserModel user, string normalizedName, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task SetUserNameAsync(UserModel user, string userName, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        #endregion

        public Task AddLoginAsync(UserModel user, UserLoginInfo login, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task RemoveLoginAsync(UserModel user, string loginProvider, string providerKey, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(UserModel user, CancellationToken cancellationToken)
        {
            return Task.FromResult((IList<UserLoginInfo>)new List<UserLoginInfo>
            {
                new UserLoginInfo(
                    DiscordAuthenticationDefaults.AuthenticationScheme,
                    user.DiscordId.ToString(),
                    user.Username
                )
            });
        }

        public async Task<UserModel> FindByLoginAsync(string loginProvider, string providerKey, CancellationToken cancellationToken)
        {
            if (loginProvider != DiscordAuthenticationDefaults.AuthenticationScheme)
            {
                return null;
            }

            return await FindByIdAsync(providerKey, cancellationToken);
        }
    }
}
