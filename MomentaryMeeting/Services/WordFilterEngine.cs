﻿namespace MomentaryMeeting.Services
{
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    public interface IWordFilterEngine
    {
        bool MessageContainsBadWord(string message);
    }

    public class WordFilterEngine : IWordFilterEngine
    {
        private List<string> _loadedBadWords;

        public WordFilterEngine(ILogger<WordFilterEngine> logger)
        {
            _loadedBadWords = new List<string>();
            var manifestNames = Assembly.GetExecutingAssembly().GetManifestResourceNames().Where(x => x.StartsWith("MomentaryMeeting.WordFilters.")).ToList();
            foreach (var manifestName in manifestNames)
            {
                using (var reader = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream(manifestName)))
                {
                    _loadedBadWords.AddRange(
                        reader.ReadToEnd().Split("\n").Select(x => x.Trim().ToLowerInvariant()).Where(x => !string.IsNullOrWhiteSpace(x)));
                }
            }
            logger.LogInformation($"Loaded {_loadedBadWords.Count} bad words into the bad word filter.");
        }

        public bool MessageContainsBadWord(string message)
        {
            return message.Split(" ").ToList().Any(x => _loadedBadWords.Contains(x.ToLowerInvariant()));
        }
    }
}
