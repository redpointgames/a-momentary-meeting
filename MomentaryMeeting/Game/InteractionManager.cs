﻿namespace MomentaryMeeting.Game
{
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Logging;
    using MomentaryMeeting.Game.Interactions;
    using MomentaryMeeting.Models;
    using MomentaryMeeting.Services;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IEndMeetings
    {
        Task EndMeeting(Meeting meeting, bool isTornApart);
    }

    public class InteractionManager : IEndMeetings
    {
        // Two players cannot meet more than once within this interval
        private const int _reMeetingLockoutInterval = 180;

        private readonly IDatabaseApi _databaseApi;
        private readonly Database _database;
        private readonly ILogger<InteractionManager> _logger;
        private readonly ArrangementGenerator _arrangementGenerator;
        private readonly IDistributedCache _distributedCache;
        private readonly IDiscordApi _discordApi;
        private List<Meeting> _meetings;
        private GameInstance _gameInstance;
        // key is player discord ID, value is a dictionary whose key is another
        // other player's discord ID and value is the UNIX timestamp when they were last together
        private Dictionary<ulong, Dictionary<ulong, long>> _lastMetRecord;

        public InteractionManager(IDatabaseApi databaseApi, ILogger<InteractionManager> logger, ArrangementGenerator arrangementGenerator, IDistributedCache distributedCache, IDiscordApi discordApi)
        {
            _databaseApi = databaseApi;
            _database = _databaseApi.Current;
            _logger = logger;
            _arrangementGenerator = arrangementGenerator;
            _distributedCache = distributedCache;
            _discordApi = discordApi;
            _meetings = new List<Meeting>();
            _lastMetRecord = new Dictionary<ulong, Dictionary<ulong, long>>();
        }

        public static long Now()
        {
            return new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
        }

        // This should be called when a meeting occurs, to record when a player
        // last met another player
        private void RecordMeetingOccurred(UserModel player, UserModel otherUser)
        {
            if (!_lastMetRecord.ContainsKey(player.DiscordId))
            {
                _lastMetRecord[player.DiscordId] = new Dictionary<ulong, long>();
            }
            _lastMetRecord[player.DiscordId][otherUser.DiscordId] = Now();
        }

        // Check whether player and otherUser have met within
        // _reMeetingLockoutInterval seconds, returns true if so
        private bool LastMeetingTooSoon(UserModel player, UserModel otherUser)
        {
            if (!_lastMetRecord.ContainsKey(player.DiscordId))
            {
                // Wow, this player has met no-one ever
                return false;
            }
            if (!_lastMetRecord[player.DiscordId].ContainsKey(otherUser.DiscordId))
            {
                // This player has never met otherUser
                return false;
            }
            long lastMetSecondsAgo = Now() - _lastMetRecord[player.DiscordId][otherUser.DiscordId];
            //_logger.LogInformation($"Players last met {lastMetSecondsAgo} s ago");
            return lastMetSecondsAgo < _reMeetingLockoutInterval;
        }

        public async Task<string> CheckForInteraction(GameInstance gameInstance, UserModel player)
        {
            if (_gameInstance == null)
                _gameInstance = gameInstance;
            var banned = await _discordApi.IsUserBanned(player.DiscordId);
            if (banned)
            {
                // Prevent a user that was banned from ever interacting with anyone else ever again. This handles scenarios where a user is banned mid-play session.
                return null;
            }
            foreach (UserModel otherUser in gameInstance.GetPlayers())
            {
                _logger.LogInformation($"ID: {player.DiscordId} {otherUser.DiscordId} {player.DiscordId != otherUser.DiscordId} object: {player != otherUser}");
                // if someone else is in the room, and they are not me and they are not in meeting and they haven't already met recently
                if ((otherUser.LocationName == player.LocationName)
                    && (player.DiscordId != otherUser.DiscordId)
                    && (otherUser.InMeeting == false)
                    && !LastMeetingTooSoon(player, otherUser))
                {
                    var otherBanned = await _discordApi.IsUserBanned(otherUser.DiscordId);
                    if (otherBanned)
                    {
                        // Prevent a user that was banned from ever interacting with anyone else ever again. This handles scenarios where a user is banned mid-play session.
                        continue;
                    }

                    player.InMeeting = true;
                    otherUser.InMeeting = true;
                    Meeting meeting = new Meeting(this);
                    meeting._users.Add(player);
                    meeting._users.Add(otherUser);
                    RecordMeetingOccurred(player, otherUser);
                    RecordMeetingOccurred(otherUser, player);
                    _meetings.Add(meeting);
                    // let them know someone has entered their room
                    await _gameInstance.SendPromptToPlayer(otherUser, "<" + _database.GlobalText.SelectFromList("begin_meeting"));
                    await _gameInstance.SyncPlayerMusic(_gameInstance.GetConnection(otherUser.DiscordId));

                    _logger.LogInformation("User(" + player.DiscordId + ":" + player.Username +
                        ") has entered a room occupied by " + "User(" + otherUser.DiscordId + ":" + otherUser.Username + "). Beginning meeting!");
                    return _database.GlobalText.SelectFromList("begin_meeting");
                }
            }
            return null;
        }

        public async Task EndMeeting(Meeting meeting, bool isTornApart)
        {
            _logger.LogInformation("Ending meeting between " + meeting._users[0] + " and " + meeting._users[1]);
            foreach (UserModel player in meeting._users)
            {
                player.InMeeting = false;
                _meetings.Remove(meeting);
                if (isTornApart)
                {
                    // send them several rooms away
                    ArrangementGenerator.Neighbours neighbours;
                    string newRoom = player.LocationName;
                    int roomDistance = 3;
                    for (int i = 0; i < roomDistance; i++)
                    {
                        neighbours = _arrangementGenerator.GetNeighbours(newRoom);
                        List<string> rooms = new List<string>();
                        string northNeighbour = neighbours.GetNeighbour(UserModel.CardinalDirection.North);
                        if (northNeighbour != "")
                            rooms.Add(northNeighbour);
                        string southNeighbour = neighbours.GetNeighbour(UserModel.CardinalDirection.South);
                        if (southNeighbour != "")
                            rooms.Add(southNeighbour);
                        string eastNeighbour = neighbours.GetNeighbour(UserModel.CardinalDirection.East);
                        if (eastNeighbour != "")
                            rooms.Add(eastNeighbour);
                        string westNeighbour = neighbours.GetNeighbour(UserModel.CardinalDirection.West);
                        if (westNeighbour != "")
                            rooms.Add(westNeighbour);
                        newRoom = rooms[new Random().Next(rooms.Count)];
                    }
                    await player.SetLocationNameAsync(_distributedCache, newRoom);
                }
                await _gameInstance.NotifyPlayerOfMeetingEnd(player.DiscordId, isTornApart);
            }
            _gameInstance.GenerateAndNotify();
        }

        public Meeting FindMeeting(UserModel aUser)
        {
            foreach (Meeting meeting in _meetings)
            {
                if (meeting._users.Contains(aUser))
                    return meeting;
            }
            _logger.LogError($"Couldn't find meeting for userid: {aUser.DiscordId}");
            return null;
        }
    }
}
