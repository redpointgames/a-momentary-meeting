﻿namespace MomentaryMeeting.Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using MomentaryMeeting.Models;

    public static class GameHelper
    {
        public static string GetRandomLocation(Database database)
        {
            var locationNames = database.Locations.Keys.ToList();
            var random = new Random();
            int index = random.Next(locationNames.Count);
            return locationNames[index];
        }

        public static UserModel.CardinalDirection GetRandomDirection()
        {
            Random rand = new Random();
            return (UserModel.CardinalDirection)rand.Next(4);
        }
    }
}
