﻿namespace MomentaryMeeting.Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using MomentaryMeeting.Models;
    using MomentaryMeeting.Services;
    using Microsoft.Extensions.Logging;

    // Generates an instance of arrangements of locations in a data structure
    // that can be conveniently traversed by player objects
    public class ArrangementGenerator
    {
        public class Neighbours
        {
            private string[] _neighbourNames;

            public Neighbours()
            {
                _neighbourNames = new string[4];
                for (int i = 0; i < 4; i++)
                {
                    _neighbourNames[i] = "";
                }
            }

            // Blank string means no link in this direction
            public string GetNeighbour(UserModel.CardinalDirection dir)
            {
                return _neighbourNames[(int)dir];
            }

            public void SetNeighbour(UserModel.CardinalDirection dir, string s)
            {
                _neighbourNames[(int)dir] = s;
            }
        }

        private Dictionary<string, Neighbours> _locations;
        private readonly Database _database;
        private readonly ILogger<ArrangementGenerator> _logger;
        Dictionary<string, string> _selectedArrangements;

        public ArrangementGenerator(IDatabaseApi databaseApi, ILogger<ArrangementGenerator> logger)
        {
            _locations = new Dictionary<string, Neighbours>();
            _database = databaseApi.Current;
            _logger = logger;
            _selectedArrangements = new Dictionary<string, string>();
        }

        // This is here for randomWeighted's sake.
        private class StringAndWeight
        {
            public string str { get; set; }
            public float weight { get; set; }
        }

        // Selected a random string from a list of strings, where each string
        // also has a weight. The weights affect the probability and they don't
        // have to be normalised.
        public static string RandomWeighted(Dictionary<string, float> possibilities)
        {
            // Build ordered list of strings, but with cumulative weights
            var cumulativeList = new StringAndWeight[possibilities.Count];
            int i = 0;
            float cumulative = 0f;
            foreach (var entry in possibilities)
            {
                var obj = new StringAndWeight();
                obj.str = entry.Key;
                obj.weight = cumulative;
                cumulative += entry.Value;
                cumulativeList[i++] = obj;
            }
            // Normalise
            for (i = 0; i < possibilities.Count; i++)
            {
                cumulativeList[i].weight /= cumulative;
            }
            // Sample from cumulative distribution
            float sample = (float)(new Random().NextDouble());
            string r = cumulativeList[possibilities.Count - 1].str;
            for (i = 0; i < possibilities.Count; i++)
            {
                if (sample < cumulativeList[i].weight)
                {
                    if (i > 0)
                    {
                        r = cumulativeList[i - 1].str;
                    }
                    else
                    {
                        // This should never happen, the first cumulative
                        // weight is always 0.0 and the minimum value of
                        // sample is 0.0, and 0.0 can't be < 0.0.
                        // But this check is here just in case.
                        r = cumulativeList[0].str;
                    }
                    break;
                }
            }
            return r;
        }

        // Establish bidirectional link between two locations
        // linkSpec is a string with the format "locationA orientation locationB"
        // orientation is the relative orientation of locationA to locationB
        // e.g. "main-road east-of beach" means main-road is east of beach
        private void LinkTwoLocations(Dictionary<string, Neighbours> locations, string linkSpec)
        {
            string[] items = linkSpec.Split(" ", StringSplitOptions.RemoveEmptyEntries);
            string locationA = items[0];
            string locationB = items[2];
            string orientation = items[1];
            UserModel.CardinalDirection dirBToA = UserModel.CardinalDirection.North; // direction needed to get from B to A
            UserModel.CardinalDirection dirAToB = UserModel.CardinalDirection.North; // direction needed to get from A to B
            bool canGoBToA = true;
            bool canGoAToB = true;

            if (orientation == "east-of")
            {
                dirBToA = UserModel.CardinalDirection.East;
                dirAToB = UserModel.CardinalDirection.West;
            }
            else if (orientation == "west-of")
            {
                dirBToA = UserModel.CardinalDirection.West;
                dirAToB = UserModel.CardinalDirection.East;
            }
            else if (orientation == "north-of")
            {
                dirBToA = UserModel.CardinalDirection.North;
                dirAToB = UserModel.CardinalDirection.South;
            }
            else if (orientation == "south-of")
            {
                dirBToA = UserModel.CardinalDirection.South;
                dirAToB = UserModel.CardinalDirection.North;
            }
            else if (orientation == "east-of-oneway")
            {
                dirBToA = UserModel.CardinalDirection.East;
                canGoAToB = false;
            }
            else if (orientation == "west-of-oneway")
            {
                dirBToA = UserModel.CardinalDirection.West;
                canGoAToB = false;
            }
            else if (orientation == "north-of-oneway")
            {
                dirBToA = UserModel.CardinalDirection.North;
                canGoAToB = false;
            }
            else if (orientation == "south-of-oneway")
            {
                dirBToA = UserModel.CardinalDirection.South;
                canGoAToB = false;
            }
            else
            {
                _logger.LogWarning($"Unknown orientation {orientation} encountered in links");
                return; // abort
            }
            if (!locations.Keys.Contains(locationA))
            {
                _logger.LogWarning($"Unknown location {locationA} encountered in links");
                return; // abort
            }
            if (!locations.Keys.Contains(locationB))
            {
                _logger.LogWarning($"Unknown location {locationB} encountered in links");
                return; // abort
            }
            if (canGoAToB)
            {
                if (!string.IsNullOrEmpty(locations[locationA].GetNeighbour(dirAToB))) _logger.LogWarning($"Overwriting link {locationA} {dirAToB} ({locations[locationA].GetNeighbour(dirAToB)}->{locationB})");
                locations[locationA].SetNeighbour(dirAToB, locationB);
            }

            if (canGoBToA)
            {
                if (!string.IsNullOrEmpty(locations[locationB].GetNeighbour(dirBToA))) _logger.LogWarning($"Overwriting link {locationB} {dirBToA} ({locations[locationB].GetNeighbour(dirBToA)}->{locationA})");
                locations[locationB].SetNeighbour(dirBToA, locationA);
            }
        }

        // Generate a new concrete arrangement of locations
        public void Generate()
        {
            // Select random arrangements
            _selectedArrangements.Clear();
            foreach (var entry in _database.Arrangements)
            {
                var selected = RandomWeighted(entry.Value);
                _selectedArrangements[entry.Key] = selected;
                _logger.LogInformation($"Selected arrangement {selected} for {entry.Key}");
            }
            // Clear out current locations list
            _locations = new Dictionary<string, Neighbours>();
            foreach (var entry in _database.Locations)
            {
                _locations[entry.Key] = new Neighbours();
            }
            // Link up the locations by examining the links
            // "Always" links are unconditional, so add all those links with no
            // additional checks
            foreach (var linkSpec in _database.Links.Always)
            {
                LinkTwoLocations(_locations, linkSpec);
            }
            // "Arrangement" links are only added if the arrangement name is set
            // to the specified value
            foreach (var entry in _database.Links.Arrangements)
            {
                string[] condition = entry.Key.Split("=");
                string arrangementName = condition[0];
                string arrangementValue = condition[1];
                // Check if the condition matches in the current set of selected
                // arrangements
                if (_selectedArrangements.Keys.Contains(arrangementName))
                {
                    if (_selectedArrangements[arrangementName] == arrangementValue)
                    {
                        foreach (var linkSpec in entry.Value)
                        {
                            LinkTwoLocations(_locations, linkSpec);
                        }
                    }
                }
                else
                {
                    _logger.LogWarning($"Link refers to {arrangementName}, which was not found in arrangements");
                }
            }
        }

        // Returns blank string if there's no link
        public string Move(string source, UserModel.CardinalDirection dir)
        {
            if (_locations.Keys.Contains(source))
            {
                return _locations[source].GetNeighbour(dir);
            }
            else
            {
                _logger.LogWarning($"Player trying to move from unknown source {source}");
                return "";
            }
        }

        public Neighbours GetNeighbours(string source)
        {
            if (_locations.Keys.Contains(source))
            {
                return _locations[source];
            }
            return null;
        }

        public string GetArrangementValue(string arrangementName)
        {
            if (_selectedArrangements.Keys.Contains(arrangementName))
            {
                return _selectedArrangements[arrangementName];
            }
            else
            {
                return "";
            }
        }
    }
}
