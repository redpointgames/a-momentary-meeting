﻿namespace MomentaryMeeting.Game.Interactions
{
    using MomentaryMeeting.Models;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class Meeting
    {
        // Meetings can never be any longer than this; players will be torn
        // apart after this
        private const int _maxMeetingLengthSeconds = 60;
        // If players spend more than this amount of time not in the same
        // location, their meeting will end
        private const int _driftApartTimeout = 30;

        public long _startTimestamp;
        public long _lastSeenTogetherTimestamp;
        public List<UserModel> _users;
        private IEndMeetings _endPoint;
        private Task _endMeetingTask;

        public static long Now()
        {
            return new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
        }

        public Meeting(IEndMeetings endPoint)
        {
            _users = new List<UserModel>();
            _startTimestamp = Now();
            _lastSeenTogetherTimestamp = _startTimestamp;
            _endPoint = endPoint;
            _endMeetingTask = Task.Run(async () =>
            {
                bool movePlayers = true;
                bool allTogether = true;

                while ((Now() - _startTimestamp) < _maxMeetingLengthSeconds)
                {
                    // Check whether players are still together
                    allTogether = true;
                    if (_users.Count > 0)
                    {
                        foreach (var player in _users)
                        {
                            if (player.LocationName != _users[0].LocationName)
                            {
                                allTogether = false;
                                break;
                            }
                        }
                    }
                    // As long as players are still together, keep updating _lastSeenTogetherTimestamp.
                    // If they aren't together, _lastSeenTogetherTimestamp will stop being updated and
                    // if they aren't together for too long the check below will fail.
                    if (allTogether)
                    {
                        _lastSeenTogetherTimestamp = Now();
                    }
                    if ((Now() - _lastSeenTogetherTimestamp) >= _driftApartTimeout)
                    {
                        movePlayers = false;
                        break;
                    }
                    await Task.Delay(500);
                }
                // players spend too long together => tear them apart
                // players go separate ways => don't tear them apart
                await _endPoint.EndMeeting(this, movePlayers);
            });
        }

        public bool IsOverYet()
        {
            return (Now() - _startTimestamp) > _maxMeetingLengthSeconds;
        }
    }
}
