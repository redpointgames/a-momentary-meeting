﻿namespace MomentaryMeeting.Game
{
    using Microsoft.Extensions.Logging;
    using MomentaryMeeting.Services;
    using MomentaryMeeting.Models;
    using System;
    using System.Collections.Generic;
    using System.Net.WebSockets;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Caching.Distributed;

    public class ConnectionInfo
    {
        public UserModel User { get; set; }
        public WebSocket WebSocket { get; set; }
        public TaskCompletionSource<object> CloseWebSocket { get; set; }
        public Task ReceiveTask { get; set; }

        public string CurrentTrack { get; set; } = "void";
        public bool CurrentInMeeting { get; set; } = false;
    }

    public class GameInstance
    {
        private Dictionary<ulong, ConnectionInfo> _players;
        private readonly IDatabaseApi _databaseApi;
        private readonly ILogger<GameInstance> _logger;
        private readonly CommandParser _commandParser;
        private ArrangementGenerator _arrangementGenerator;
        private readonly IDistributedCache _distributedCache;
        private readonly IDiscordApi _discordApi;
        private Task _arrangementGeneratorTask;

        public GameInstance(IDatabaseApi databaseApi, ILogger<GameInstance> logger, CommandParser commandParser, ArrangementGenerator arrangementGenerator, IDistributedCache distributedCache, IDiscordApi discordApi)
        {
            _players = new Dictionary<ulong, ConnectionInfo>();
            _databaseApi = databaseApi;
            _logger = logger;
            _commandParser = commandParser;
            _arrangementGenerator = arrangementGenerator;
            _distributedCache = distributedCache;
            _discordApi = discordApi;
            // TODO: put this in a better place
            // Also schedule it to be called at some interval
            _arrangementGenerator.Generate();

            _arrangementGeneratorTask = Task.Run(async () =>
                {
                    while (true)
                    {
                        await Task.Delay(1000 * 60 * 15);
                        GenerateAndNotify();
                    }
                });
        }

        public void GenerateAndNotify()
        {
            _arrangementGenerator.Generate();
            foreach (var kv in _players)
            {
                // No need to wait for anything here, ju st make a best-effort
                // broadcast to all players
                _ = this.SendPromptToPlayer(kv.Value, ">" + _databaseApi.Current.GlobalText.SelectFromList("world_shift"));
            }
        }

        public async Task AcceptPlayer(UserModel incomingUser, WebSocket webSocket, TaskCompletionSource<object> socketFinishedTsc)
        {
            var banned = await _discordApi.IsUserBanned(incomingUser.DiscordId);
            if (banned)
            {
                _logger.LogInformation($"Rejecting player {incomingUser.DiscordId}:{incomingUser.Username} because they are banned.");
                socketFinishedTsc.SetResult(new object());
                return;
            }

            _logger.LogInformation($"Accepting player {incomingUser.DiscordId}:{incomingUser.Username}");

            await _discordApi.LogUserConnected(incomingUser.DiscordId);

            _players[incomingUser.DiscordId] = new ConnectionInfo
            {
                User = incomingUser,
                WebSocket = webSocket,
                CloseWebSocket = socketFinishedTsc,
            };

            var existingLocation = await _distributedCache.GetStringAsync("loc-" + incomingUser.DiscordId);
            if (existingLocation != null)
            {
                await incomingUser.SetLocationNameAsync(_distributedCache, existingLocation);
            }
            else
            {
                var desiredLocation = GameHelper.GetRandomLocation(_databaseApi.Current);
                while (_databaseApi.Current.Locations[desiredLocation].NoSpawn)    // tries to set a spawnable location
                {
                    desiredLocation = GameHelper.GetRandomLocation(_databaseApi.Current);
                }
                await incomingUser.SetLocationNameAsync(_distributedCache, desiredLocation);
            }

            await SyncPlayerMusic(_players[incomingUser.DiscordId]);
            await SendPromptToPlayer(_players[incomingUser.DiscordId], ">" + _commandParser.GetRoomDescription(incomingUser));

            _players[incomingUser.DiscordId].ReceiveTask = Task.Run(async () =>
                 {
                     try
                     {
                         var buffer = new byte[1024 * 4];
                         var result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                         while (!result.CloseStatus.HasValue)
                         {
                             if (result.MessageType == WebSocketMessageType.Text && result.EndOfMessage)
                             {
                                 await ProcessInputFromPlayer(_players[incomingUser.DiscordId], Encoding.UTF8.GetString(buffer, 0, result.Count));
                             }

                             result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                         }
                         await webSocket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
                         socketFinishedTsc.TrySetResult(new object());
                     }
                     catch (Exception ex)
                     {
                         _logger.LogError("Uncaught exception in receive loop for WebSocket: " + ex.Message + "\n" + ex.StackTrace);
                     }
                     finally
                     {
                         _players.Remove(incomingUser.DiscordId);
                         await _discordApi.LogUserDisconnected(incomingUser.DiscordId);
                     }
                 });

            //return Task.CompletedTask;
        }

        public async Task SendPromptToPlayer(UserModel user, string text)
        {
            ConnectionInfo theConnection = null;
            foreach (var conn in _players.Values)
                if (conn.User == user)
                    theConnection = conn;
            await SendPromptToPlayer(theConnection, text);
        }

        public async Task SendPromptToPlayer(ConnectionInfo connection, string text)
        {
            var buffer = Encoding.UTF8.GetBytes(text);
            await connection.WebSocket.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
        }

        public async Task ProcessInputFromPlayer(ConnectionInfo connection, string input)
        {
            _logger.LogInformation($"Received input from player {connection.User.DiscordId}:{connection.User.Username}: {input}");

            string response = await _commandParser.Parse(connection.User, input, this);
            await SyncPlayerMusic(connection);
            await SendPromptToPlayer(connection, response);
        }


        public List<UserModel> GetPlayers()
        {
            List<UserModel> users = new List<UserModel>();
            foreach (var conn in _players.Values)
                users.Add(conn.User);
            return users;
        }

        public ConnectionInfo GetConnection(ulong id)
        {
            return _players[id];
        }

        public async Task SyncPlayerMusic(ConnectionInfo connection)
        {
            var location = _databaseApi.Current.Locations[connection.User.LocationName];
            if (string.IsNullOrWhiteSpace(location.Region))
            {
                _logger.LogError($"Location {connection.User.LocationName} does not have a region set!");
                return;
            }

            var isInMeeting = connection.User.InMeeting;

            if (connection.CurrentTrack != location.Region ||
                connection.CurrentInMeeting != isInMeeting)
            {
                await SendPromptToPlayer(connection, "settrack:" + location.Region + ":" + isInMeeting);
                connection.CurrentTrack = location.Region;
                connection.CurrentInMeeting = isInMeeting;
            }
        }

        public async Task NotifyPlayerOfMeetingEnd(ulong discordId, bool isTornApart)
        {
            ConnectionInfo conn = GetConnection(discordId);
            string term;
            if (isTornApart)
            {
                term = _databaseApi.Current.GlobalText.SelectFromList("end_meeting_torn");
                term += "\n\n" + _commandParser.GetRoomDescription(conn.User);
                await SyncPlayerMusic(conn);
                await SendPromptToPlayer(conn, term);
            }
            else
            {
                term = ">" + _databaseApi.Current.GlobalText.SelectFromList("end_meeting_apart");
                await SyncPlayerMusic(conn);
                await SendPromptToPlayer(conn, term);
            }
        }
    }
}
