﻿namespace MomentaryMeeting.Game
{
    using Microsoft.Extensions.Logging;
    using MomentaryMeeting.Services;
    using MomentaryMeeting.Models;
    using System.Text;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Hosting;
    using MomentaryMeeting.Game.Interactions;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using static MomentaryMeeting.Game.GameInstance;
    using static MomentaryMeeting.Models.UserModel;

    public class CommandParser
    {
        private readonly ILogger<CommandParser> _logger;
        // Converts textual move directions (e.g. "forward") into UserModel.PlayerMoves
        private readonly Dictionary<string, UserModel.RelativeDirection> _validMoveDirections;
        // Converts textual turn directions (e.g. "left") into UserModel.PlayerMoves
        private readonly Dictionary<string, UserModel.RelativeDirection> _validTurnDirections;
        private readonly IDatabaseApi _databaseApi;
        private ArrangementGenerator _arrangementGenerator;
        private InteractionManager _interactionManager;
        private readonly IHostEnvironment _hostEnvironment;
        private readonly IDistributedCache _distributedCache;
        private readonly IWordFilterEngine _wordFilterEngine;
        private readonly IDiscordApi _discordApi;
        private readonly Database _database;

        public CommandParser(ILogger<CommandParser> logger, IDatabaseApi databaseApi, ArrangementGenerator arrangementGenerator, InteractionManager interactionManager, IHostEnvironment hostEnvironment, IDistributedCache distributedCache, IWordFilterEngine wordFilterEngine, IDiscordApi discordApi)
        {
            _databaseApi = databaseApi;
            _database = _databaseApi.Current;

            _logger = logger;
            _arrangementGenerator = arrangementGenerator;
            _validMoveDirections = new Dictionary<string, UserModel.RelativeDirection>() {
                { "forward", UserModel.RelativeDirection.Forward },
                { "forwards", UserModel.RelativeDirection.Forward },
                { "ahead", UserModel.RelativeDirection.Forward },
                { "backwards", UserModel.RelativeDirection.Back },
                { "backward", UserModel.RelativeDirection.Back },
                { "back", UserModel.RelativeDirection.Back },
                { "behind", UserModel.RelativeDirection.Back },
                { "left", UserModel.RelativeDirection.Left },
                { "right", UserModel.RelativeDirection.Right }
            };
            _validTurnDirections = new Dictionary<string, UserModel.RelativeDirection>() {
                { "left", UserModel.RelativeDirection.Left },
                { "right", UserModel.RelativeDirection.Right },
                { "around", UserModel.RelativeDirection.Back },
                { "backwards", UserModel.RelativeDirection.Back },
                { "backward", UserModel.RelativeDirection.Back },
                { "back", UserModel.RelativeDirection.Back }
            };

            _interactionManager = interactionManager;
            _hostEnvironment = hostEnvironment;
            _distributedCache = distributedCache;
            _wordFilterEngine = wordFilterEngine;
            _discordApi = discordApi;
        }

        private static bool IsCardinal(string s)
        {
            switch (s.ToLower())
            {
                case "north":
                case "east":
                case "south":
                case "west":
                    return true;
                default:
                    return false;
            }
        }

        public async Task<string> Parse(UserModel player, string command, GameInstance gameInstance)
        {
            string[] words = command.Split(" ", StringSplitOptions.RemoveEmptyEntries);
            string response = "";

            if (words.Length == 0)
            {
                response = _database.GlobalText.SelectFromList("idle");
            }
            else
            {
                string verb = words[0].ToLower();

                switch (verb)
                {
                    case "move":
                    case "go":
                    case "walk":
                    case "run":
                        {
                            if (words.Length < 2)
                            {
                                response = _database.GlobalText.SelectFromList("no_subject").Replace("__VERB__", verb);
                            }
                            else
                            {
                                string direction = words[1].ToLower();
                                if (_validMoveDirections.Keys.Contains(direction))
                                {
                                    // Translate from direction to cardinal direction.
                                    // For example, if player is facing East, then "move back" will
                                    // cause RelativeCardinalDirection to return West.
                                    var playerDir = player.RelativeCardinalDirection(_validMoveDirections[direction]);
                                    _logger.LogInformation($"A player facing {player.CurrentDirection} went {_validMoveDirections[direction]} and is now facing {playerDir}");
                                    var newLocation = _arrangementGenerator.Move(player.LocationName, playerDir);
                                    player.CurrentDirection = playerDir;

                                    if (newLocation == "")
                                    {
                                        response = _database.GlobalText.SelectFromList("cannot_do").Replace("__VERB__", verb).Replace("__SUBJECT__", direction);
                                    }
                                    else
                                    {
                                        response = _database.GlobalText.SelectFromList("can_do").Replace("__VERB__", verb).Replace("__SUBJECT__", direction)
                                            + "{.5}\n\n";
                                        _logger.LogInformation($"Moving player to new location {newLocation}");
                                        await player.SetLocationNameAsync(_distributedCache, newLocation);
                                        response += GetRoomDescription(player);
                                        response += "\n\n" + await _interactionManager.CheckForInteraction(gameInstance, player);
                                    }
                                }
                                else
                                {
                                    if (IsCardinal(direction))
                                    {
                                        response = _database.GlobalText.SelectFromList("cardinal_subject").Replace("__VERB__", verb).Replace("__SUBJECT__", direction);
                                    }
                                    else
                                    {
                                        response = _database.GlobalText.SelectFromList("bad_subject").Replace("__VERB__", verb).Replace("__SUBJECT__", direction);
                                    }
                                }
                            }
                        }
                        break;
                    case "inspect":
                        {
                            var location = _databaseApi.Current.Locations[player.LocationName];
                            if (location.Inspectables != null)
                            {
                                var visibleInspectables = location.Inspectables.SelectMany(x =>
                                {
                                    var prompt = EvaluatePrompt(x.Value.Prompt);
                                    return new[] { x.Key }.ToDictionary(k => k, v => prompt);
                                }).ToDictionary(k => k.Key, v => v.Value);

                                if (words.Length < 2)
                                {
                                    if (visibleInspectables.Count == 0)
                                    {
                                        response = _database.GlobalText.SelectFromList("inspect_nothing");
                                    }
                                    else
                                    {
                                        response = _database.GlobalText.SelectFromList("inspect") + "{.5}\n\n";
                                        foreach (var kv in visibleInspectables)
                                        {
                                            response += "- {b}" + kv.Key + "{/b}{.5}\n";
                                        }
                                    }
                                }
                                else
                                {
                                    var target = words[1];
                                    if (visibleInspectables.ContainsKey(target))
                                    {
                                        response = visibleInspectables[target];
                                    }
                                    else
                                    {
                                        response = _database.GlobalText.SelectFromList("inspect_notfound");
                                    }
                                }
                            }
                            else
                            {
                                response = _database.GlobalText.SelectFromList("inspect_nothing");
                            }
                            break;
                        }
                    case "turn":
                    case "rotate":
                    case "face":
                    case "look":
                        {
                            if (words.Length < 2)
                            {
                                if (verb == "look")
                                {
                                    response = _database.GlobalText.SelectFromList("look") + "\n\n";
                                    // TODO: fix quick and dirty 'look' at location
                                    response += GetRoomDescription(player);
                                }
                                else
                                {
                                    response = _database.GlobalText.SelectFromList("look_where").Replace("__VERB__", verb);
                                }
                            }
                            else
                            {
                                string direction = words[1].ToLower();

                                if (_validTurnDirections.Keys.Contains(direction))
                                {
                                    response = _database.GlobalText.SelectFromList("can_do").Replace("__VERB__", verb).Replace("__SUBJECT__", direction)
                                            + "{.5}\n\n";
                                    _logger.LogInformation($"Turning player {direction}");
                                    player.CurrentDirection = player.RelativeCardinalDirection(_validTurnDirections[direction]);
                                    _logger.LogInformation($"Player is now facing {player.CurrentDirection}");
                                }
                                else
                                {
                                    if (IsCardinal(direction))
                                    {
                                        response = _database.GlobalText.SelectFromList("cardinal_subject").Replace("__VERB__", verb).Replace("__SUBJECT__", direction);
                                    }
                                    else
                                    {
                                        response = _database.GlobalText.SelectFromList("bad_subject").Replace("__VERB__", verb).Replace("__SUBJECT__", direction);
                                    }
                                }
                            }
                        }
                        break;
                    case "say":
                        {
                            // TODO: whatever functionality this was supposed to be (refresh db before getting yaml:path info?)
                            // await _databaseApi.Refresh();
                            if (!player.InMeeting)
                            {
                                response = _database.GlobalText.SelectFromList("lonely");
                            }
                            else if (words.Length < 2)
                            {
                                response = _database.GlobalText.SelectFromList("say_what").Replace("__VERB__", verb);
                            }
                            else if (words[1].StartsWith($"yaml:") && _hostEnvironment.IsDevelopment())
                            {
                                response += $"YAML GET!\n";
                                string logInfo = $"YAML command activated! "; // to be passed to logger at end

                                string yamlPath = words[1].Split(":")[1]; // get path after 'yaml:'

                                string[] yamlTerms = yamlPath.Contains(".") ? yamlPath.Split(".") : yamlPath.Split("/");  // split path into terms

                                if (yamlTerms.Length < 2)
                                {
                                    response += $"ERROR: Too few yaml terms. Requires at least a first and second level term.\n" +
                                        $"For example: global_text.test";
                                    logInfo = $"But there were too few yaml terms.";
                                    _logger.LogInformation(logInfo); // early break
                                    break;

                                }

                                // get dictionary for top level term (global text, region, arrangement, location)
                                if (yamlTerms[0] == "global_text") //global_text
                                {
                                    logInfo = $"Accessed [{yamlTerms[0]}]. ";
                                    // get value from query
                                    if (_database.GlobalText.ContainsKey(yamlTerms[1]))
                                    {
                                        response += $"Output of [{yamlPath}]: \n" +
                                            $"{_database.GlobalText[yamlTerms[1]]}";    // eg: test
                                        logInfo += $"Accessed [{yamlTerms[1]}]";
                                    }
                                    else
                                    {
                                        response += $"ERROR: No \'{yamlTerms[1]}\' value found underneath global_text.";
                                        logInfo += $"But there was no [{yamlTerms[1]}] value found.";
                                    }
                                }
                                else if (yamlTerms[0] == "regions")
                                {
                                    logInfo = $"Accessed [{yamlTerms[0]}]. ";
                                    // get value from query
                                    if (_database.Regions.ContainsKey(yamlTerms[1]))
                                    {
                                        Region reg = _database.Regions[yamlTerms[1]];

                                        // when no next level specified
                                        if (yamlTerms.Length < 2)
                                        {
                                            response += $"ERROR: Too few terms for {yamlTerms[0]}/{yamlTerms[1]}";
                                            logInfo += "But there were too few yaml terms."; // early break
                                            _logger.LogInformation(logInfo);
                                            break;
                                        }


                                        if (yamlTerms[2] == "description")// get description 
                                        {
                                            response += $"Output of {yamlPath}:\n" +
                                            $"{reg.Description}";
                                            logInfo += "Logged output successfully!";
                                        }

                                        else if (yamlTerms[2] == "edge_links") // get edge links
                                        {
                                            response += $"Output of {yamlPath}:\n";
                                            // print out key value pairs for edge links
                                            foreach (KeyValuePair<string, string> val in reg.EdgeLinks)
                                            {
                                                response += $"  - {val.Key} : {val.Value}\n";
                                            }
                                        }
                                        else
                                        {
                                            // error message
                                            response += $"ERROR: The {yamlTerms[2]} value get is not implemented!";
                                        }
                                    }
                                    else
                                    {
                                        response += $"ERROR: No \'{yamlTerms[1]}\' value found underneath {yamlTerms[0]}.";
                                        logInfo += $"But there was no [{yamlTerms[1]}] value found.";
                                    }
                                }
                                else if (yamlTerms[0] == "arrangements")
                                {
                                    logInfo = $"Accessed [{yamlTerms[0]}]. ";
                                    // get value from query
                                    if (_database.Arrangements.ContainsKey(yamlTerms[1]))
                                    {
                                        Arrangement arr = _database.Arrangements[yamlTerms[1]];
                                        // print all key value pairs for this arrangement
                                        if (arr.Values.Count > 0)
                                        {
                                            response += $"Output of {yamlPath}:\n";
                                            foreach (KeyValuePair<string, float> val in arr)
                                            {
                                                response += $"  - {val.Key} : {val.Value}\n";
                                            }
                                        }
                                        else
                                        {
                                            response += $"ERROR: No Key Value pairs underneath {yamlTerms[0]}/{yamlTerms[1]}";
                                            logInfo += $"But there were no values underneath [{yamlTerms[0]}/{yamlTerms[1]}]";
                                        }
                                    }
                                    else
                                    {
                                        response += $"ERROR: No \'{yamlTerms[1]}\' value found underneath {yamlTerms[0]}.";
                                        logInfo += $"But there was no [{yamlTerms[1]}] value found.";
                                    }
                                }
                                else if (yamlTerms[0] == "locations")
                                {
                                    // TODO: get location yaml

                                    logInfo = $"Accessed [{yamlTerms[0]}]. ";
                                    if (_database.Locations.ContainsKey(yamlTerms[1])) // query the locations label
                                    {
                                        Location loc = _database.Locations[yamlTerms[1]];

                                        // check if more info requested
                                        if (yamlTerms.Length < 3)
                                        {
                                            response += $"{yamlTerms[1]} exists, try accessing .region or .prompt underneath it.";
                                            break;
                                        }

                                        // get region
                                        if (yamlTerms[2] == "region")
                                        {
                                            response += $"Output of {yamlPath}: {loc.Region}";
                                        }
                                        else if (yamlTerms[2] == "prompt")
                                        {
                                            response += $"Output of {yamlPath}:\n";
                                            // check if prompts set
                                            if (loc.Prompt.Count > 0)
                                            {
                                                foreach (Prompt p in loc.Prompt) // give list
                                                {
                                                    string type = "  - Type: ";
                                                    if (p.Condition.Type == ConditionType.Always)
                                                    {
                                                        // state that it's always
                                                        type += "Always\n";
                                                    }
                                                    else if (p.Condition.Type == ConditionType.Arrangement)
                                                    {
                                                        type += "Arrangement";

                                                        // list each arrangement
                                                        if (p.Condition.Arrangements.Count > 0)
                                                        {
                                                            foreach (ConditionArrangement ca in p.Condition.Arrangements)
                                                            {
                                                                response += $"    - {ca.Name} : {ca.Value}";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            // no arrangements set
                                                            response += "    - [None Set!]";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        // unimplemented type
                                                        type += "UNIMPLEMENTED\n";
                                                    }
                                                    response += type;

                                                    string txt = "  - Text:\n" + p.Text;
                                                    response += txt;
                                                    response += "\n\n";
                                                }
                                            }
                                            else
                                            {
                                                response += $"There are no prompts set for {yamlPath[1]}";
                                            }
                                        }
                                        else if (yamlTerms[2] == "inspectables")
                                        {
                                            response += $"Output of {yamlPath}:\n";
                                            // check if inspectables set
                                            if (loc.Inspectables == null)
                                            {
                                                response += $"There are no inspectables set for {yamlPath[1]}";
                                            }
                                            else
                                            {
                                                foreach (KeyValuePair<string, Inspectable> ins in loc.Inspectables)
                                                {
                                                    response += $"  - INSPECTABLE '{ins.Key}'\n";

                                                    // join aliases
                                                    string aliases = string.Join(", ", ins.Value.Aliases);
                                                    response += $"    - Aliases: {aliases}\n";

                                                    //loop through prompt data
                                                    foreach (Prompt p in ins.Value.Prompt)
                                                    {
                                                        response += $"    - PROMPT:\n";
                                                        string type = "      - Type: ";
                                                        if (p.Condition.Type == ConditionType.Always)
                                                        {
                                                            // state that it's always
                                                            type += "Always\n";
                                                        }
                                                        else if (p.Condition.Type == ConditionType.Arrangement)
                                                        {
                                                            type += "Arrangement";

                                                            // list each arrangement
                                                            if (p.Condition.Arrangements.Count > 0)
                                                            {
                                                                foreach (ConditionArrangement ca in p.Condition.Arrangements)
                                                                {
                                                                    response += $"        - {ca.Name} : {ca.Value}";
                                                                }
                                                            }
                                                            else
                                                            {
                                                                // no arrangements set
                                                                response += "        - [None Set!]";
                                                            }
                                                        }
                                                        else
                                                        {
                                                            // unimplemented type
                                                            type += "UNIMPLEMENTED\n";
                                                        }
                                                        response += type;

                                                        string txt = "      - Text:\n" + p.Text;
                                                        response += txt;
                                                        response += "\n\n";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        response += $"{yamlTerms[1]} could not be found in {yamlTerms[0]}, check spelling?";
                                    }
                                }
                                else if (yamlTerms[0] == "links")
                                {
                                    // TODO: get links yaml

                                    // query the links label

                                }
                                else
                                {
                                    response += $"ERROR: Not a valid top level term.\n" +
                                        $"Current top levels are:\n" +
                                        $" - global_text\n" +
                                        $" - regions\n" +
                                        $" - arrangements\n" +
                                        $" - locations\n" +
                                        $" - links\n";
                                    logInfo += "But the top level term wasn't valid.";
                                }

                                _logger.LogInformation(logInfo);
                            }
                            else
                            {
                                string content = command.Substring(verb.Length + 1);
                                if (_wordFilterEngine.MessageContainsBadWord(content))
                                {
                                    await _discordApi.LogUserMessage(player.DiscordId, content, true);

                                    string message = "<" + _database.GlobalText.SelectFromList("other_says_blocked");
                                    Meeting meeting = _interactionManager.FindMeeting(player);
                                    foreach (UserModel other in meeting._users)
                                    {
                                        if (other.DiscordId != player.DiscordId)
                                        {
                                            ConnectionInfo conn = gameInstance.GetConnection(other.DiscordId);
                                            await gameInstance.SendPromptToPlayer(conn, message);
                                        }
                                    }
                                    response += ">" + _database.GlobalText.SelectFromList("you_say_blocked");
                                }
                                else
                                {
                                    await _discordApi.LogUserMessage(player.DiscordId, content, false);

                                    string message = "<" + _database.GlobalText.SelectFromList("other_says").Replace("__CONTENT__", content);
                                    Meeting meeting = _interactionManager.FindMeeting(player);
                                    foreach (UserModel other in meeting._users)
                                    {
                                        if (other.DiscordId != player.DiscordId)
                                        {
                                            ConnectionInfo conn = gameInstance.GetConnection(other.DiscordId);
                                            await gameInstance.SendPromptToPlayer(conn, message);
                                        }
                                    }
                                    response += ">" + _database.GlobalText.SelectFromList("you_say").Replace("__CONTENT__", content);
                                }
                            }
                        }
                        break;
                    case "help":    // help
                        {
                            response = $"You aren't sure what to do,{{.5}} and feel as though you should ask for help.{{1}}\n\n" +  // double {{ escapes the character in interpolated string
                                $"  help        - Lists available keyword commands.\n" +
                                $"  say         - Say whatever is typed after the keyword.\n" +
                                $"  look        - Observe the area.\n" +
                                $"              - When used with a direction, faces you in that direction\n" +
                                $"                AKA: face, rotate, turn (Note: only when used with direction)\n" +
                                $"  inspect     - Lists objects in the area that you can interact with.\n" +
                                $"              - When used with additional keywords, interacts with the object.\n" +
                                $"  run         - Moves in a direction (forward, left, right, back)\n" +
                                $"                AKA: walk, go, move\n\n";

                            if (_hostEnvironment.IsDevelopment())
                            {
                                response += $"DEVELOPER ENVIRONMENT DETECTED!!!\n" +
                                    $"Here are some additional developer commands:\n" +
                                    $"say (ext)     - Say is extended to print some yaml values when used as follows:\n" +
                                    $"              'say yaml:path' where 'path' is the path of the yaml file.\n" +
                                    $"              eg: 'say yaml:global_text.test', 'say yaml:location.liquor_store.prompt'\n" +
                                    $"todo          - Lists all code and yaml 'TODO's\n" +
                                    $"teleport      - When used with a location label, it teleports you to that room.\n\n";
                            }
                        }
                        break;
                    default:
                        {
                            if (_hostEnvironment.IsDevelopment())
                            {
                                switch (verb)
                                {
                                    case "todo":
                                        response = GetTodos();
                                        break;
                                    case "teleport":
                                        {
                                            if (words.Length < 2)
                                            {
                                                response = $"You have not specified a location to {verb} to...";
                                                break;
                                            }
                                            // check location exists
                                            if (_database.Locations.ContainsKey(words[1]))
                                            {
                                                // if it does, change player location (user model) to location
                                                string newLoc = words[1];

                                                response = $"Through powers unknown, you instantaneously teleport from {player.LocationName} to {newLoc}!\n\n";
                                                await player.SetLocationNameAsync(_distributedCache, newLoc);

                                                // then return new location
                                                response += GetRoomDescription(player);
                                                response += "\n\n" + await _interactionManager.CheckForInteraction(gameInstance, player);
                                            }
                                            else
                                            {
                                                // if it doesn't, advise user
                                                response = $"{words[1]} does not exist, you cannot {verb} there...";
                                            }


                                        }
                                        break;
                                    default:
                                        response = _database.GlobalText.SelectFromList("unknown_verb").Replace("__VERB__", verb);
                                        break;
                                }
                            }
                            else
                            {
                                response = _database.GlobalText.SelectFromList("unknown_verb").Replace("__VERB__", verb);
                            }
                        }
                        break;
                }
            }

            return response;

        }

        public string GetRoomDescription(UserModel player)
        {
            var sb = new StringBuilder();
            List<Prompt> prompts = _database.Locations[player.LocationName].Prompt;
            sb.Append(EvaluatePrompt(prompts));
            var neighbours = _arrangementGenerator.GetNeighbours(player.LocationName);
            AutoExitForDirection(RelativeDirection.Forward);
            AutoExitForDirection(RelativeDirection.Right);
            AutoExitForDirection(RelativeDirection.Back);
            AutoExitForDirection(RelativeDirection.Left);

            _logger.LogInformation($"For a player facing {player.CurrentDirection}, the following mappings apply: north -> {player.DirectionAsString(_databaseApi, CardinalDirection.North, false)}, south -> {player.DirectionAsString(_databaseApi, CardinalDirection.South, false)}, east -> {player.DirectionAsString(_databaseApi, CardinalDirection.East, false)}, west -> {player.DirectionAsString(_databaseApi, CardinalDirection.West, false)}");

            return sb.ToString().Replace("{North}", player.DirectionAsString(_databaseApi, CardinalDirection.North, false))
                               .Replace("{East}", player.DirectionAsString(_databaseApi, CardinalDirection.East, false))
                               .Replace("{South}", player.DirectionAsString(_databaseApi, CardinalDirection.South, false))
                               .Replace("{West}", player.DirectionAsString(_databaseApi, CardinalDirection.West, false))
                               .Replace("{north}", player.DirectionAsString(_databaseApi, CardinalDirection.North, true))
                               .Replace("{east}", player.DirectionAsString(_databaseApi, CardinalDirection.East, true))
                               .Replace("{south}", player.DirectionAsString(_databaseApi, CardinalDirection.South, true))
                               .Replace("{west}", player.DirectionAsString(_databaseApi, CardinalDirection.West, true));

            void AutoExitForDirection(RelativeDirection forward)
            {
                var dir = (CardinalDirection)(int)player.RelativeCardinalDirection(forward);
                if (_database.Locations.ContainsKey(neighbours.GetNeighbour(dir)))
                {
                    var target = _database.Locations[neighbours.GetNeighbour(dir)];
                    if (target.ViewFrom != null && target.ViewFrom.ContainsKey(dir))
                    {
                        sb.AppendLine().Append(EvaluatePrompt(target.ViewFrom[dir]));
                    }
                    else if (!string.IsNullOrWhiteSpace(target.ViewFromAny))
                    {
                        if (dir == CardinalDirection.North)
                            sb.AppendLine().Append(target.ViewFromAny.Replace("{dir}", "{north}").Replace("{Dir}", "{North}"));
                        else if (dir == CardinalDirection.East)
                            sb.AppendLine().Append(target.ViewFromAny.Replace("{dir}", "{east}").Replace("{Dir}", "{East}"));
                        else if (dir == CardinalDirection.South)
                            sb.AppendLine().Append(target.ViewFromAny.Replace("{dir}", "{south}").Replace("{Dir}", "{South}"));
                        else if (dir == CardinalDirection.West)
                            sb.AppendLine().Append(target.ViewFromAny.Replace("{dir}", "{west}").Replace("{Dir}", "{West}"));
                    }
                }
            }
        }

        public string EvaluatePrompt(List<Prompt> prompts)
        {
            foreach (var prompt in prompts)
            {
                if (IsConditionTrue(prompt.Condition))
                {
                    return prompt.Text;
                }
            }
            return null;
        }

        public bool IsConditionTrue(Condition condition)
        {
            if (condition.Type == ConditionType.Always)
                return true;
            foreach (var c in condition.Arrangements)
            {
                if (_arrangementGenerator.GetArrangementValue(c.Name) != c.Value)
                    return false;
            }
            return true;
        }

        private string GetTodos()
        {
            // TODO: delete the todo command
            string result = "";
            string wanted_path = Path.GetDirectoryName(Directory.GetCurrentDirectory());
            foreach (string file in Directory.GetFiles(wanted_path, "*", SearchOption.AllDirectories))
            {
                if (file.EndsWith(".cs") || file.EndsWith("site.js") || file.EndsWith(".yaml"))
                {
                    string[] lines = File.ReadAllLines(file);
                    int index = 0;
                    foreach (string line in lines)
                    {
                        index++;
                        if (line.ToLower().Contains("todo") && (line.Contains("//") || line.Contains("#"))) // Oh no, this line sees itself
                        {
                            // filename:line "comment"
                            string fileName = file[wanted_path.Length..];
                            string comment = fileName + ":" + index + " \"" + line.Trim() + "\"";
                            result += "\n" + comment;
                        }
                    }
                }
            }
            return result;
        }
    }
}
