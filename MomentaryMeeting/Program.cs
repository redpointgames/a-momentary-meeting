namespace MomentaryMeeting
{
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using MomentaryMeeting.Services;
    using Redpoint.CloudFramework;
    using System.Threading;
    using System.Threading.Tasks;

    public class Program
    {
        public static async Task Main(string[] args)
        {
            ThreadPool.SetMinThreads(4, 4);

            await CloudFramework.WebApp
                .UsePrefixProvider<MomentaryMeetingPrefixProvider>()
                .UseStartup<Startup>()
                .DisableGoogleCloud()
                .StartWebApp();
        }
    }
}
